var CloudLeaf = (function($) {
	var captchas,
		context,
		//index,
		labels,
		map,
		options,
		slides;

	function CloudLeaf(params) {
		var option, value;
		options = {
			'marker': false,
			'unveil-offset': 0
		};
		if (typeof params === 'object') {
	        for (option in params) {
	        	value = params[option];
	        	options[option] = value;
        	}
		}
		context = this;

		this.init();
	}

	CloudLeaf.prototype.init = function() {
		// Header search box
		if ($('.search-box').length) {
			$('.search-box').on('focus', function() {
				$(this).parent().parent().addClass('open');
			}).on('blur', function() {
				$(this).parent().parent().removeClass('open');
			});
		}
		// Unveil image-box
		if ($('.image-box').length) {
			this.initUnveil();
		}
		// Activate baloon
		if ($('.box-baloon').length) {
			this.initBaloons();
		}
		// Slides [deprecated]
		/*
		if ($('.slides-container').length) {
			this.initSlides();
			$(window).on('load', this._resizeSlides);
		}
		*/
		// Slides [new]
		if ($('.slide-container').length) {
			this.initSlides();
			$(window).on('load', this._resizeSlides);
		}
		// Contacts page
		if ($('.site-inner').hasClass('contacts')) {
			this.initContacs();
			$(window).on('load', this.initRecaptcha);
		}
		// Google maps
		if ($('#map').length) {
			this.initMap();
		}
		// Cloud icons
		if ($('.cloud-icons').length) {
			this.initIcons();
		}
		// Homepage feats
		if ($('.feat').length) {
			this.initFeats();
		}

		// Homepage temp
		if ($('.hover-button').length) {
			$('.hover-button').on('mouseenter click', function() {
				$('.hover-img').addClass('active');
			}).on('mouseleave', function() {
				$('.hover-img').removeClass('active');
			})
		}

	}

	/**** Slides ***/
	CloudLeaf.prototype.initSlides = function() {
		context.slides = new Array();

		$('.slide-container').each(function(index) {
			$(this).attr('id', 'slider-' + index);

			context.slides[index] = {
				index: 0,
				total: $(this).find('.slide-content').length,
				divid: '#slider-' + index
			};

			$(this).find('.slide-arrow').on('click', context._onClickSlideArrow);
			$(this).find('.slide-index').on('click', context._onClickSlideIndex);
		});

		context._resizeSlides();
	}
	CloudLeaf.prototype.updateSlides = function(index) {
		var container = $(context.slides[index].divid);

		for (var i = 0; i < context.slides[index].total; ++i) {
			if (i == context.slides[index].index) {
				container.find('.slide-' + i).removeClass('hidden');
			} else {
				container.find('.slide-' + i).addClass('hidden');
			}
		}

		if (container.find('.slide-index-cont').length) {
			container.find('.slide-index').each(function() {
				if ($(this).data('index') == context.slides[index].index)
					$(this).addClass('selected');
				else
					$(this).removeClass('selected');
			});
		}
	}
	CloudLeaf.prototype._onClickSlideArrow = function() {
		var index = parseInt($(this).parent().attr('id').replace('slider-', ''));

		if ($(this).hasClass('prev')) {
			context.slides[index].index -= 1;
			if (context.slides[index].index < 0) context.slides[index].index += context.slides[index].total;
		} else {
			context.slides[index].index += 1;
			context.slides[index].index = context.slides[index].index % context.slides[index].total;
		}

		context.updateSlides(index);
	}
	CloudLeaf.prototype._onClickSlideIndex = function() {
		var index = parseInt($(this).parent().parent().attr('id').replace('slider-', ''));

		context.slides[index].index = $(this).data('index');

		context.updateSlides(index);
	}
	CloudLeaf.prototype._resizeSlides = function() {
		for (var i = 0; i < context.slides.length; ++i) {
			context.updateSlides(i);
		}
	}
	/*
	CloudLeaf.prototype.initSlides = function() {
		context.index  = 0;
		context.slides = $('.slides-container').find('.slide').length;

		$('.slides-container').find('.button').on('click', context.onClickSlidesButton);

		context._resizeSlides();
		context.updateSlides();
	}

	CloudLeaf.prototype.updateSlides = function() {
		var current, position, max = context.slides > 3 ? 2 : 1;
		$('.slides-container').find('.slide').each(function() {
			current  = $(this).data('index');
			position = current - context.index;
			// looping
			if (position > max) position -= context.slides;
			if (position < -max) position += context.slides;
			// normalize
			position = Math.min(max, Math.max(-max, position));

			switch (position) {
				case -2: // pprev
					if ($(this).hasClass('next') || $(this).hasClass('nnext')) $(this).removeClass('animated');
					else $(this).addClass('animated');
					$(this)
						.addClass('pprev')
						.removeClass('prev')
						.removeClass('next')
						.removeClass('nnext')
						.addClass('hide');
					break;
				case -1: // prev
					if ($(this).hasClass('next') || $(this).hasClass('nnext')) $(this).removeClass('animated');
					else $(this).addClass('animated');
					$(this)
						.removeClass('pprev')
						.addClass('prev')
						.removeClass('next')
						.removeClass('nnext')
						.addClass('hide');
					break;
				case 0: //current
					$(this)
						.removeClass('pprev')
						.removeClass('prev')
						.removeClass('next')
						.removeClass('nnext')
						.removeClass('hide')
						.addClass('animated');
					break;
				case 1: // next
					if ($(this).hasClass('prev') || $(this).hasClass('pprev')) $(this).removeClass('animated');
					else $(this).addClass('animated');
					$(this)
						.removeClass('pprev')
						.removeClass('prev')
						.addClass('next')
						.removeClass('nnext')
						.addClass('hide');
					break;
				case 2: // nnext
					if ($(this).hasClass('prev') || $(this).hasClass('pprev')) $(this).removeClass('animated');
					else $(this).addClass('animated');
					$(this)
						.removeClass('pprev')
						.removeClass('prev')
						.removeClass('next')
						.addClass('nnext')
						.addClass('hide');
					break;
			}
		});
	}

	CloudLeaf.prototype.onClickSlidesButton = function(evt) {
		context._prevent(evt);

		if ($(this).hasClass('prev')) {
			context.index = --context.index % context.slides;
			if (context.index < 0) context.index += context.slides;
		} else if ($(this).hasClass('next')) {
			context.index = ++context.index % context.slides;
		}

		context.updateSlides();
	}

	CloudLeaf.prototype._resizeSlides = function() {
		var slideH = 0;
		$('.slide').find('img').each(function() {
			slideH = Math.max(slideH, $(this).outerHeight());
		});
		$('.slides-container, .slides-content, .slide, .slides-nav .button').css({
			height: slideH + 'px'
		});
	}
	*/
	/*** /Slides ***/

	/**** Contacts ***/
	CloudLeaf.prototype.initContacs = function() {
		$('#header-kit').on('click', context.onClickContactMenu);
		$('.contacts-menu li a').on('click', context.onClickContactMenu);
		var hash = window.location.hash.substr(1), found = false;
		$('.contacts-menu li a').each(function() {
			if ($(this).attr('href').substr(1) == hash) {
				$(this).click();
				found = true;
			}
		});
		if (hash.length == 0 || !found) $('.contacts-menu li a').first().click();

		context.labels = { };
		$('.contacts-content textarea').each(function() {
			context.labels[$(this).attr('id')] = $(this).val();
			$(this).on('focus', function() {
				if ($(this).val() == context.labels[$(this).attr('id')]) {
					$(this).val('');
				}
				$(this).addClass('active');
			});
			$(this).on('blur', function() {
				if ($(this).val() == '' || $(this).val() == context.labels[$(this).attr('id')]) {
					$(this).val(context.labels[$(this).attr('id')]);
					$(this).removeClass('active');
				}
			});
		});

		$('.contacts-content select').on('change', function() {
			var newVal = $(this).find('option:selected').text();
			var selId = $(this).attr('id');

	        $('#' + selId + '-span').html(newVal);
		});

		$('.contacts-content form').on('submit', context.onSubmitContactForm);
	}

	CloudLeaf.prototype.onClickContactMenu = function(evt) {
		context._prevent(evt);

		var form = context._getStringAfterChar($(this).attr('href'), '#');
		// Menu
		$('.contacts-menu li').removeClass('active');
		$('.contacts-menu .form-active').attr('id', form);
		$('.contacts-menu li a').each(function() {
			if (context._getStringAfterChar($(this).attr('href'), '#') == form)
				$(this).parent().addClass('active');
		});

		// Form
		var cHeight = 0;
		$('.form-container').each(function() {
			if ($(this).attr('id') == form) {
				$(this).removeClass('hide').addClass('active');
				cHeight = $(this).outerHeight();
			} else {
				$(this).removeClass('active').addClass('hide');
			}
		});
		$('.contacts-content').css('height', cHeight + 'px');
	}

	CloudLeaf.prototype.onSubmitContactForm = function() {
		var fields = { }, can_send = true;
		$(this).find('input, textarea').each(function() {
			if ($(this).attr('name')) {
				if ($(this).attr('type') == 'checkbox') {
					if ($(this).data('type') == 'mandatory') {
						if (!$(this).is(':checked')) {
							$(this).parent().addClass('mandatory');
							can_send = false;
						} else {
							$(this).parent().removeClass('mandatory');
						}
					}
				} else {
					if ($(this).data('type') == 'mandatory') {
						if ($(this).val() == '' || $(this).val() == undefined || $(this).val() == context.labels[$(this).attr('id')]) {
							$(this).addClass('mandatory');
							can_send = false;
						} else {
							if ($(this).attr('type') == 'email') {
								if (!context._validateEmail($(this).val())) {
									$(this).addClass('error');
									can_send = false;
								} else {
									$(this).removeClass('error mandatory');
								}
							} else {
								$(this).removeClass('mandatory error');
							}
						}
					}
				}

				//console.log("Mandatory: " + $(this).data('type') + ", Type: " + $(this).attr('type') + ", Name: " + $(this).attr('name') + ", Value: '" + $(this).val() + "'");
				fields[$(this).attr('name')] = $(this).val();
			}
		});

		// check captcha
		var captcha = grecaptcha.getResponse(captchas[$(this).find('.g-recaptcha').attr('id')]);

		// Sending form
		if (can_send && captcha.length > 0) {
			// Disable all fields
			$(this).find('input, textarea').prop('disabled', true);
			var msg_box = $(this).find('.message');
			var data = {
				'action':  'cloudleaf_contact_us',
				'captcha': captcha,
				'pid':     ajax_var.ajaxnonce,
			}
			for (var name in fields) {
				data[name] = fields[name];
			}

			$.ajax({
				type: 'POST',
				url:  ajax_var.ajaxurl,
				data: data,
				success: function(response) {
					console.log("Success", response);

					msg_box.html(response.response == 'ok' ? msg_box.data('ok') : msg_box.data('ko'));
				},
				error: function(response) {
					console.log("Error", response);
					$(this).find('input, textarea').prop('disabled', false);

					msg_box.html(msg_box.data('error'));
				},
				dataType: 'json'
			});
		} else {

		}

		// TODO : rivedere respond

		return false;
	}

	CloudLeaf.prototype.initRecaptcha = function() {
		captchas = { };
		$('.contacts-content .g-recaptcha').each(function() {
			captchas[$(this).attr('id')] = grecaptcha.render($(this).attr('id'), {'sitekey' : $(this).data('sitekey')});
		});
	}
	/*** /Contacts ***/

	/**** Gmap ***/
	CloudLeaf.prototype.initMap = function() {
		var lat    = $('#map').data('lat'),
			lng    = $('#map').data('lng'),
			token  = 'pk.eyJ1Ijoic3Vic2lyZSIsImEiOiJjaXFnZWgxeDUwMDlpaTFreHp2amMwbXg3In0.Y9nwxvYEUbIOemaAXr9RTQ',
			marker = options.marker ? L.icon( options.marker ) : false;

		map = L.map('map', {
			center: [lat, lng],
			zoom: 15,
			scrollWheelZoom: false,
			zoomControl: false,
			attributionControl: false
		});
		L.tileLayer('https://api.mapbox.com/styles/v1/subsire/ciqgevpc0000zecnhecu2dgk4/tiles/256/{z}/{x}/{y}?access_token={accessToken}', {
			//attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
			//id: 'subsire.0keldk33',
			accessToken: token
		}).addTo(map);
		if (marker) L.marker([lat, lng], {icon: marker}).addTo(map);
		else L.marker([lat, lng]).addTo(map);
		map.panBy([0, 155], { animate: false });
	}
	/*** /Gmap ***/

	/**** Unveil ***/
	CloudLeaf.prototype.initUnveil = function() {
		var wHeight = $(window).height() + $(window).scrollTop() - options['unveil-offset'];
		options['unveil-count'] = 0;

		$('.image-box').each(function() {
			if ($(this).offset().top > wHeight) {
				$(this).addClass('hidden');
				options['unveil-count']++;
			}
		});

		if (options['unveil-count'] > 0) $(window).on('scroll', this.scrollHandler);
	}

	CloudLeaf.prototype.checkUnveil = function() {
		if (options['unveil-count'] > 0) {
			var wHeight = $(window).height() + $(window).scrollTop() - options['unveil-offset'];

			$('.image-box.hidden').each(function() {
				if ($(this).offset().top < wHeight) {
					$(this).removeClass('hidden');
					options['unveil-count']--;
				}
			});
		}
	}
	/*** /Unveil ***/

	/**** Baloons ***/
	CloudLeaf.prototype.initBaloons = function() {
		$('.box-baloon').on('click mouseenter', function() {
			$(this).find('.baloon-container').addClass('show');
		}).on('mouseleave', function() {
			$(this).find('.baloon-container').removeClass('show');
		});
	}
	/*** /Baloons ***/

	/**** Cloud icons ***/
	CloudLeaf.prototype.initIcons = function() {
		$('.cloud-icons-container').css({
			height: ($('.cloud-icons').height() + 10) + 'px'
		});
		$('.cloud-icons').css({
			//height: $('.cloud-icons').height() + 'px',
			position: 'absolute'
		});
		$('.cloud-icon').on('click', this._openIcon);
	}

	CloudLeaf.prototype._openIcon = function() {
		var text = $(this).next();
		if (text.hasClass('active')) {
			$('.cloud-icons').css( 'left', 0 );
			text.removeClass('active');
		} else {
			$('.cloud-icons').css( 'left', -($(this).data('index') * 159) + 'px' );
			text.addClass('active');
		}
	}
	/*** /Cloud icons ***/

	/**** Homepage feats ***/
	CloudLeaf.prototype.initFeats = function() {
		$('.feat-button').on('click', this._openFeat);
	}

	CloudLeaf.prototype._closeFeat = function() {
		$(this).off('click', context._closeFeat).removeClass('close');
		var feat = $(this).parent();

		feat.find('.feat-content').fadeTo(200, 0.01, function() {
			$(this).hide();

			$('.feat').each(function() {
				if ($(this).attr('id') != feat.attr('id'))
					$(this).stop().fadeTo(200, 1.00);
			});
		});

		$(this).on('click', context._openFeat);
	}

	CloudLeaf.prototype._openFeat = function() {
		$(this).off('click', context._openFeat).addClass('close');
		var feat = $(this).parent();

		$('.feat').each(function() {
			if ($(this).attr('id') != feat.attr('id'))
				$(this).stop().fadeTo(200, 0.01, function() { $(this).hide(); });
		});

		// TODO: ehrrrr
		setTimeout(function() { feat.find('.feat-content').fadeTo(200, 1.00); }, 200);

		$(this).on('click', context._closeFeat);
	}
	/*** /Homepage feats ***/

	CloudLeaf.prototype.scrollHandler = function() {
		context.checkUnveil();
	}

	CloudLeaf.prototype._getStringAfterChar = function(string, char) {
		return string.substr(string.indexOf(char) + 1)
	}

	CloudLeaf.prototype._prevent = function(event) {
		if (!event) event = window.event;
		if (event) {
			if (event.preventDefault) event.preventDefault();
			else event.returnValue = false;
		}
	}

	CloudLeaf.prototype._validateEmail = function(email) {
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    	return re.test(email);
	}

	return CloudLeaf;

})(jQuery);
