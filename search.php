<?php
/**
 * The template for displaying search results pages
 *
 * @package WordPress
 * @subpackage CloudLeaf
 * @since CloudLeaf 1.0
 */

get_header(); ?>

    <div class="search-container cage">
        <h1>Search results for <strong><?php echo get_search_query(); ?></strong></h1>

        <?php
        $args = array(
            'post_type' => 'page',
            'order' => 'ASC',
            'orderby' => 'menu_order parent',
            'post__not_in' => array( 18, 161 ),
        );
        $query = new WP_Query($args);

        $search = get_search_query();
        $results = array();
        if ($query->have_posts()) :
            while ($query->have_posts()) :
                $query->the_post();

                $page = array();
                // title
                $pos = stripos($post->post_title, $search);
                if ($pos !== FALSE) $page['title'] = $post->post_title;
                // content
                if (($content = search_content($post->post_content, $search)) != false) $page['content'] = $content;
                // meta fields
                if (!isset($page['content'])) {
                    $fields = get_field_objects($post->ID);
                    $found  = false;
                    foreach ($fields as $name => $value) {
                        switch ($value['type']) {
                            case 'text':
                            case 'textarea':
                            case 'wysiwyg':
                                if (($content = search_content($value['value'], $search)) != false) {
                                    $page['content'] = $content;
                                    $found = true;
                                }
                                break;
                            case 'repeater':
                                $f_names = array();
                                foreach ($value['sub_fields'] as $sub_field) {
                                    if ($sub_field['type'] == 'text' || $sub_field['type'] == 'textarea' || $sub_field['type'] == ['wysiwyg']) {
                                        $f_names[] = $sub_field['name'];
                                    }
                                }
                                if (count($f_names)) {
                                    foreach ($value['value'] as $sub_value) {
                                        foreach($f_names as $f_name) {
                                            if (!$found) {
                                                if (!empty($sub_value[$f_name])) {
                                                    if (($content = search_content($sub_value[$f_name], $search)) != false) {
                                                        $page['content'] = $content;
                                                        $found = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                break;
                        }

                        if ($found) break;
                    }
                    if (isset($page['content']))
                        $page['fields'] = $fields;
                }

                if (count($page)) {
                    $page['title'] = $post->post_title;
                    $page['permalink'] = get_permalink();
                    if (!isset($page['content'])) $page['content'] = '';
                    $page['content'] = str_ireplace($search, "<strong>". ucfirst($search) . "</strong>", $page['content']);
                    $page['count'] = count($page);
                    $results[] = $page;
                }
            endwhile;
            wp_reset_postdata();
        endif;

        foreach ($results as $result) :
        ?>
        <a href="<?php echo $result['permalink']; ?>">
            <div class="search-result">
                <h4><?php echo apply_filters('the_title', $result['title']); ?></h4>
                <p><?php echo str_ireplace(array("<br>", "<br/>", "<br />"), " ", $result['content']); ?></p>
            </div>
        </a>
        <?php
        endforeach;
        ?>
    </div>

<?php get_footer(); ?>
