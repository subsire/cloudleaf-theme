<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage CloudLeaf
 * @since CloudLeaf 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="/manifest.json">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="apple-mobile-web-app-title" content="CloudLeaf">
	<meta name="application-name" content="CloudLeaf">
	<meta name="theme-color" content="#ffffff">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>

	<script type="text/javascript">
	var cloudleaf;
	jQuery(document).ready(function() {
		var params = {
			'marker': {
				iconUrl: '<?php echo get_template_directory_uri(); ?>/img/marker.png',
				iconSize: [51, 76],
				iconAnchor: [25.5, 76]
			},
			'unveil-offset': 50
		};
		cloudleaf = new CloudLeaf(params);
	});
	</script>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<div class="site-inner<?php if (basename(get_page_template()) == 'contacts.php') { ?> contacts<?php } ?>">

		<header id="masthead" class="site-header<?php if (is_front_page()) echo " dark"; ?>" role="banner">
			<div class="header-nav">
				<div class="logo">
					<a href="<?php bloginfo('url'); ?>/">
						<img src="<?php echo get_template_directory_uri(); ?>/img/logo<?php if (is_front_page()) echo "-white"; ?>.png" title="<?php bloginfo('name'); ?>" />
					</a>
				</div>
				<nav class="main-menu">
					<ul>
						<?php
						$post_id    = isset($post) && !is_search() ? $post->ID : null;
						$parent     = isset($post) && !is_search() ? $post->post_parent : null;
						$locations  = get_nav_menu_locations();
						$primary    = wp_get_nav_menu_object($locations['primary']);
						$items      = wp_get_nav_menu_items($primary->term_id);
						$index      = 0;

						$menu_items = array();
						foreach ($items as $item) {
							if ($item->menu_item_parent == 0) {
								$menu_items[] = (object)array(
									'item_id'   => $item->ID,
									'post_id'   => $item->object_id,
									'is_last'   => (++$index == count($items)),
									'permalink' => get_permalink($item->object_id),
									'title'     => $item->title,
									'is_active' => ($item->object_id == $post_id || $item->object_id == $parent),
									'children'  => array(),
								);
							} else {
								foreach ($menu_items as $menu_item) {
									if ($menu_item->item_id == $item->menu_item_parent) {
										$menu_item->children[] = (object)array(
											'item_id'   => $item->ID,
											'post_id'   => $item->object_id,
											'permalink' => get_permalink($item->object_id),
											'title'     => $item->title,
											'is_active' => ($item->object_id == $post_id || $item->object_id == $parent),
										);

										break;
									}
								}
							}
						}

						foreach ($menu_items as $item) : ?>
						<li<?php if ($item->is_active) { ?> class="active"<?php } ?>>
							<a href="<?php echo $item->permalink; ?>" title="<?php echo $item->title; ?>">
								<span><?php echo $item->title; ?></span>
							</a>
							<?php if (count($item->children) > 0) : ?>
							<ul class="item-children">
								<?php foreach ($item->children as $child) : ?>
								<li<?php if ($child->is_active) { ?> class="active"<?php } ?>>
									<a href="<?php echo $child->permalink; ?>"><?php echo $child->title; ?></a>
								</li>
								<?php endforeach; ?>
								<div class="triangle"></div>
							</ul>
							<?php endif; ?>
						</li>
						<?php endforeach; ?>
					</ul>
				</nav>
			</div>
		</header><!-- .site-header -->

		<div id="content" class="site-content">
