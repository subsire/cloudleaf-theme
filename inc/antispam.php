<?php
/*
 * AntiSpam class
 *
 */

if (!class_exists('antispam')) :

class antispam
{
    // vars

    /*
     * Constructor
     *
     */
    function __construct() {

    }

    /*** Publics ***/

    public function check_external_link($links, $blogurl) {
        $result = false;
        foreach ($links as $link) {
            if (stristr($link, $blogurl) === false) {
                $result = true;
            }
        }

        return $result;
    }

    public function check_recaptcha($captcha_response, $remoteip = null) {
        // Construct the Google verification API request link
        $params = array(
            'secret'   => RECAPTCHA_SECRET,
            'response' => $captcha_response,
        );
        if (isset($remoteip)) $params['remoteip'] = $remoteip;

        // Get cURL resource
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL            => RECAPTCHA_VERIFYURL,
            CURLOPT_POST           => 1,
            CURLOPT_POSTFIELDS     => http_build_query($params),
        ));

        // Send the request
        $response = curl_exec($curl);
        // Close request to clear up some resources
        curl_close($curl);

        $response = @json_decode($response, true);

        return $response['success'];
    }

    public function parse_links($haystack, $type = 'url') {
    	/**
    	 *  Parse a body of content for links - extracts URLs and Anchor Text
    	 *  $type: 'url' for URLs, 'domain' for just Domains, 'url_at' for URLs from Anchor Text Links only, 'anchor_text' for Anchor Text
    	 *  Returns an array
    	 */
    	$parse_links_regex = "~(<\s*a\s+[a-z0-9\-_\.\?\='\"\:\(\)\{\}\s]*\s*href|\[(url|link))\s*\=\s*['\"]?\s*(https?\://[a-z0-9\-_\/\.\?\&\=\~\@\%\+\#\:]+)\s*['\"]?\s*[a-z0-9\-_\.\?\='\"\:;\(\)\{\}\s]*\s*(>|\])([a-z0-9Ã Ã¡Ã¢Ã£Ã¤Ã¥Ã§Ã¨Ã©ÃªÃ«Ã¬Ã­Ã®Ã¯Ã±Å„Å†ÅˆÃ²Ã³Ã´ÃµÃ¶Ã¹ÃºÃ»Ã¼\-_\/\.\?\&\=\~\@\%\+\#\:;\!,'\(\)\{\}\s]*)(<|\[)\s*\/\s*a\s*(>|(url|link)\])~iu";
    	$search_http_regex = "~(?:^|\s+)(https?\://[a-z0-9\-_\/\.\?\&\=\~\@\%\+\#\:]+)(?:$|\s+)~iu";
    	preg_match_all( $parse_links_regex, $haystack, $matches_links, PREG_PATTERN_ORDER );
    	$parsed_links_matches 			= $matches_links[3]; /* Array containing URLs parsed from Anchor Text Links in haystack text */
    	$parsed_anchortxt_matches		= $matches_links[5]; /* Array containing Anchor Text parsed from Anchor Text Links in haystack text */

        if ($type === 'url' || $type === 'domain') {
    		$url_haystack = preg_replace( "~\s~", ' - ', $haystack ); /* Workaround Added 1.3.8 */
    		preg_match_all( $search_http_regex, $url_haystack, $matches_http, PREG_PATTERN_ORDER );
    		$parsed_http_matches 		= $matches_http[1]; /* Array containing URLs parsed from haystack text */
    		$parsed_urls_all_raw 		= array_merge( $parsed_links_matches, $parsed_http_matches );
    		$parsed_urls_all			= array_unique( $parsed_urls_all_raw );
    		if ($type === 'url') {
                $results = $parsed_urls_all;
            } elseif ($type === 'domain') {
    			$parsed_urls_all_domains = array();
    			foreach ($parsed_urls_all as $u => $url_raw) {
    				$url = $this->casetrans( 'lower', trim( stripslashes( $url_raw ) ) );
    				if (empty( $url )) { continue; }
    				$domain = $this->get_domain( $url );
    				if (!in_array( $domain, $parsed_urls_all_domains, TRUE )) { $parsed_urls_all_domains[] = $domain; }
    			}
    			$results = $parsed_urls_all_domains;
    		}
    	} elseif ($type === 'url_at') {
            $results = $parsed_links_matches;
        } elseif ($type === 'anchor_text') {
            $results = $parsed_anchortxt_matches;
        }

    	return $results;
    }

    public function sanitize_string($string) {
        /* Sanitize a string. Much faster than sanitize_text_field() */
        return trim( addslashes( htmlentities( stripslashes( strip_tags( $string ) ) ) ) );
    }

    /*** Privates ***/

    private function casetrans($type, $string) {
    	/**
    	 *  Convert case using multibyte version if available, if not, use defaults
    	 *  Added 1.8.4
    	 */
    	switch ($type) {
    		case 'upper':
    			return function_exists( 'mb_strtoupper' ) ? mb_strtoupper( $string, 'UTF-8' ) : strtoupper( $string );
    		case 'lower':
    			return function_exists( 'mb_strtolower' ) ? mb_strtolower( $string, 'UTF-8' ) : strtolower( $string );
    		case 'ucfirst':
    			if( function_exists( 'mb_strtoupper' ) && function_exists( 'mb_substr' ) ) {
    				$strtmp = mb_strtoupper( mb_substr( $string, 0, 1, 'UTF-8' ), 'UTF-8' ) . mb_substr( $string, 1, NULL, 'UTF-8' );
    				/* 1.9.5.1 - Added workaround for strange PHP bug in mb_substr() on some servers */
    				return $this->strlen( $string ) === $this->strlen( $strtmp ) ? $strtmp : ucfirst( $string );
    			} else { return ucfirst( $string ); }
    		case 'ucwords':
    			return function_exists( 'mb_convert_case' ) ? mb_convert_case( $string, MB_CASE_TITLE, 'UTF-8' ) : ucwords( $string );
    			/**
    			 *  Note differences in results between ucwords() and this.
    			 *  ucwords() will capitalize first characters without altering other characters, whereas this will lowercase everything, but capitalize the first character of each word.
    			 *  This works better for our purposes, but be aware of differences.
    			 */
    		default:
    			return $string;
    	}
    }

    private function fix_url($url = NULL, $rem_frag = FALSE, $rem_query = FALSE, $rev = FALSE) {
    	/**
    	 *  Fix poorly formed URLs so as not to throw errors or cause problems
    	 */
    	if (empty( $url )) { return ''; }
    	$url = trim( $url );
    	/* Too many forward slashes or colons after http */
    	$url = preg_replace( "~^(https?)\:+/+~i", "$1://", $url);
    	/* Too many dots */
    	$url = preg_replace( "~\.+~i", ".", $url);
    	/* Too many slashes after the domain */
    	$url = preg_replace( "~([a-z0-9]+)/+([a-z0-9]+)~i", "$1/$2", $url);
    	/* Remove fragments */
    	if( !empty( $rem_frag ) && strpos( $url, '#' ) !== FALSE ) { $url_arr = explode( '#', $url ); $url = $url_arr[0]; }
    	/* Remove query string completely */
    	if( !empty( $rem_query ) && strpos( $url, '?' ) !== FALSE ) { $url_arr = explode( '?', $url ); $url = $url_arr[0]; }
    	/* Reverse */
    	if( !empty( $rev ) ) { $url = strrev($url); }
    	return $url;
    }

    private function get_domain($url, $email_domain = FALSE) {
    	/**
    	 *  Get domain from URL
    	 *  Filter URLs with nothing after http
    	 *  $email_domain will run through rs_wpss_get_email_domain()
    	 */
    	if (empty( $url ) || preg_match( "~^https?\:*/*$~i", $url )) { return ''; }
    	/* Fix poorly formed URLs so as not to throw errors when parsing */
    	$url = $this->fix_url( $url );
    	/* NOW start parsing */
    	$parsed = @$this->parse_url( $url );
    	/* Filter URLs with no domain */
    	if( empty( $parsed['host'] ) ) { return ''; }
    	$domain = $this->casetrans( 'lower', $parsed['host'] );
    	if( !empty( $email_domain ) ) { $domain = $this->get_email_domain( $domain ); }
    	return $domain;
    }

    private function get_email_domain( $domain ) {
    	/**
    	 *  Get email domain for use in email addresses
    	 *  Strip 'www.' & 'm.' from beginning of domain
    	 */
    	if( empty( $domain ) ) { return ''; }
    	$domain = preg_replace( "~^(ww[w0-9]|m)\.~i", '', $domain );
    	return $domain;
    }

    private function is_wp_ver($ver) {
    	/**
    	 *  Check if current install is a specific WordPress version or later, for compatibility checks
    	 *  Added 1.9.5.7
    	 */
        global $wp_version;

    	return version_compare( $wp_version, $ver, '>=' );
    }

    private function parse_url( $url ) {
    	/**
    	 *  Use this function instead of parse_url() for compatibility with PHP < 5.4.7. wp_parse_url() was added in WP ver 4.4
    	 *  Added 1.9.8.4
    	 */
        return ( function_exists( 'wp_parse_url' ) && $this->is_wp_ver('4.4') ) ? wp_parse_url( $url ) : parse_url( $url );
    }

    private function strlen( $string ) {
    	/**
    	 *  Use this function instead of mb_strlen because some servers (often IIS) have mb_ functions disabled by default
    	 *  BUT mb_strlen is superior to strlen, so use it whenever possible
    	 */
    	return function_exists( 'mb_strlen' ) ? mb_strlen( $string, 'UTF-8' ) : strlen( $string );
    }
}

endif;
