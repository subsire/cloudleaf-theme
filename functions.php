<?php
/**
 * Twenty Sixteen functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage CloudLeaf
 * @since CloudLeaf 1.0
 */


if ( ! function_exists( 'cloudleaf_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * Create your own twentysixteen_setup() function to override in a child theme.
 *
 * @since Cloudleaf 1.0
 */
function wordpress_setup() {
	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );


	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary'   => __( 'Primary Menu', 'cloudleaf' ),
		'secondary' => __( 'Secondary Menu' , 'cloudleaf' ),
		'kit'       => __( 'Kit Menu', 'cloudleaf' ),
	) );


}
endif;
add_action( 'after_setup_theme', 'wordpress_setup' );



/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Cloudleaf 1.0
 */
function wordpress_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'wordpress_javascript_detection', 0 );

/**
 * Enqueues scripts and styles.
 *
 * @since Cloudleaf 1.0
 */
function wordpress_scripts() {

	// Theme stylesheet.
	//wp_enqueue_style( 'cloudleaf-style', get_stylesheet_uri() );
	wp_enqueue_style( 'cloudleaf-style', get_template_directory_uri() . '/css/main.css' );

	// Site main script
	wp_enqueue_script( 'cloudleaf-js', get_template_directory_uri() . '/js/cloudleaf.min.js', array('jquery'), '1.0.0' );

	// Ajax data
	$data = array(
        'ajaxurl' => admin_url('admin-ajax.php'),
        'ajaxnonce' => wp_create_nonce('cloudleaf-ajax-check'),
    );
    wp_localize_script( 'cloudleaf-js', 'ajax_var', $data );

	// Contacts page
	if (basename(get_page_template()) == 'contacts.php') {
		wp_enqueue_style( 'leaflet-css', 'http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.css' );
		wp_enqueue_script( 'leaflet-js', 'http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.js' );
		wp_enqueue_script( 'google-captcha', 'https://www.google.com/recaptcha/api.js?render=explicit' );
	}
	// ReCaptcha

}
add_action( 'wp_enqueue_scripts', 'wordpress_scripts' );


/**
 * Converts a HEX value to RGB.
 *
 * @since Twenty Sixteen 1.0
 *
 * @param string $color The original color, in 3- or 6-digit hexadecimal form.
 * @return array Array containing RGB (red, green, and blue) values for the given
 *               HEX code, empty array otherwise.
 */
function twentysixteen_hex2rgb( $color ) {
	$color = trim( $color, '#' );

	if ( strlen( $color ) === 3 ) {
		$r = hexdec( substr( $color, 0, 1 ).substr( $color, 0, 1 ) );
		$g = hexdec( substr( $color, 1, 1 ).substr( $color, 1, 1 ) );
		$b = hexdec( substr( $color, 2, 1 ).substr( $color, 2, 1 ) );
	} else if ( strlen( $color ) === 6 ) {
		$r = hexdec( substr( $color, 0, 2 ) );
		$g = hexdec( substr( $color, 2, 2 ) );
		$b = hexdec( substr( $color, 4, 2 ) );
	} else {
		return array();
	}

	return array( 'red' => $r, 'green' => $g, 'blue' => $b );
}

function cloudleaf_image_size() {
	add_image_size( 'cloudleaf_1024', 1024, 9999 );
	add_image_size( 'cloudleaf_1_3', 341, 341, true );
	add_image_size( 'cloudleaf_thumb', 200, 200, true );

}
add_action( 'after_setup_theme', 'cloudleaf_image_size' );

function cloudleaf_admin_menu() {
	remove_menu_page('edit.php');
    remove_menu_page('edit-comments.php');
}
add_action( 'admin_menu', 'cloudleaf_admin_menu' );

// disable the emoji's
function disable_emojis() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	remove_filter( 'wp_mail', ' wp_staticize_emoji_for_email' );
	add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
}
add_action( 'init', 'disable_emojis' );

function disable_emojis_tinymce( $plugins ) {
	if (is_array($plugins)) {
		return array_diff($plugins, array('wpemoji'));
	} else {
		return array();
	}
}

// ajax
function cloudleaf_contact_us() {
	check_ajax_referer('cloudleaf-ajax-check', 'pid');

	include_once('inc/antispam.php');

	$json = array();
	$antispam = new antispam();

	// check recaptcha
    if (!$antispam->check_recaptcha($_POST['captcha'], $_SERVER['REMOTE_ADDR'])) {
        $json['result']  = 'ko';
        $json['message'] = 'unable to validate recaptcha';

        echo json_encode($json);
        wp_die();
    }

	$contact_id = 8;
    $page_email = get_post_meta($contact_id, $_POST['type'] . '_email', true);
	$send_to    = $page_email ? $page_email : false;

	// check send-to email
	if (!$send_to) {
		$json['result']  = 'ko';
		$json['message'] = 'send-to email not valid';

		echo json_encode($json);
		wp_die();
	}

	// check send-from email
    if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        $json['result']  = 'ko';
        $json['message'] = 'email not valid';

        echo json_encode($json);
        wp_die();
    }

	// spam (if contains external urls)
    $links = $antispam->parse_links($_POST['message']);
    $spam  = $antispam->check_external_link($links, get_bloginfo('url'));

	// already used fields
	$fields = array( 'captcha', 'type', 'name', 'email', 'pid', 'action' );

	$from = array(
        'mail' => $_POST['email'],
        'name' => $antispam->sanitize_string($_POST['name']),
    );
	$subject  = ucfirst(str_replace('_', ' ', $_POST['type']));
	$message  = "Message sent from: " . $from['name'] . " <" . $from['mail'] . ">\n\n";
	foreach ($_POST as $key => $value) {
		if (!in_array($key, $fields)) {
			$message .= ucfirst($antispam->sanitize_string($key)) . "\n";
			$message .= $antispam->sanitize_string($value) . "\n\n";
		}
	}
	$message .= $antispam->sanitize_string($_POST['message']);
	$result   = wp_mail($send_to, $subject, $message);

	$json['result'] = $result ? 'ok' : 'ko';
	$json['spam']   = $spam ? 1 : 0;

	echo json_encode($json);
    wp_die();
}
add_action('wp_ajax_cloudleaf_contact_us', 'cloudleaf_contact_us');
add_action('wp_ajax_nopriv_cloudleaf_contact_us', 'cloudleaf_contact_us');


/** Theme's Api **/

function get_kit_menu($hash = null) {
	$locations = get_nav_menu_locations();
	$primary   = wp_get_nav_menu_object($locations['kit']);
	$items     = wp_get_nav_menu_items($primary->term_id);
	$item      = $items[0];
	$hash      = empty($hash) ? '#request-a-kit' : $hash;

	return (object)array(
		'link'  => get_permalink($item->object_id) . $hash,
		'title' => $item->title,
	);
}

function search_content($content, $search) {
	if (($pos = stripos($content, $search)) !== FALSE) {
		$start = max(0, $pos - 20);
		$length = max(strlen($search) + 20, 100);

		$result  = ($start > 0) ? "..." : "";
		$result .= substr($content, $start, $length);
		$result .= ($length < strlen($content)) ? "..." : "";

		return $result;
	}

	return false;
}


/** Advanced Custom Fields **/

include_once('inc/advanced-custom-fields/acf.php');
include_once('inc/acf-repeater/acf-repeater.php');
//include_once('inc/acf-installer.php');

//define( 'ACF_LITE', true );
