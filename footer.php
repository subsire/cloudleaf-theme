<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage CloudLeaf
 * @since CloudLeaf 1.0
 */
?>

		</div><!-- .site-content -->

		<footer id="colophon" class="site-footer" role="contentinfo">
			<div class="footer-background">
				<div class="footer-menu cage">
					<div class="footer-logo">
						<img src="<?php echo get_template_directory_uri(); ?>/img/logo-white.png" title="<?php bloginfo('name'); ?>" />
					</div>
					<div class="secondary-menu">
						<ul>
							<?php
							$locations = get_nav_menu_locations();
							$primary = wp_get_nav_menu_object($locations['secondary']);
							$items = wp_get_nav_menu_items($primary->term_id);
							$index = 0;
							foreach ($items as $item) {
								echo "<li";
								if (++$index == count($items)) echo " class=\"last\"";
								echo ">";
								echo "<a href=\"" . get_permalink($item->object_id) . "\">" . $item->title . "</a>";
								echo "</li>";
							}
							?>
						</ul>
					</div>
					<?php $homepage_id = get_option('page_on_front'); ?>
					<div class="footer-info">
						<div class="footer-left">
							<?php echo apply_filters('the_content', get_post_meta($homepage_id, 'footer_left_content', true)); ?>
						</div>
						<div class="footer-right">
							<?php echo apply_filters('the_content', get_post_meta($homepage_id, 'footer_right_content', true)); ?>
						</div>
					</div>
				</div>
			</div>
		</footer><!-- .site-footer -->

	</div><!-- .site-inner -->
</div><!-- .site -->

<?php wp_footer(); ?>
</body>
</html>
