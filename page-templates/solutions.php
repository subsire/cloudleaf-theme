<?php
/**
 * Template Name: Solutions Template
 *
 * @package WordPress
 * @subpackage CloudLeaf
 * @since CloudLeaf 1.0
 */

$id    = get_the_ID();
$metas = get_post_meta($id);

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<div class="solutions-intro">
    <div class="cage">
        <div class="column">
            <div class="table-container">
                <div class="table-content">
                    <h1><?php echo str_replace(array('<p>', '</p>'), '', apply_filters('the_content', $metas['intro_content'][0])); ?></h1>
                </div>
            </div>
        </div><!--
        --><div class="column">
            <?php
            $imgid = intval($metas['intro_image'][0]);
            if ($imgid) :
                $image = wp_get_attachment_image_url($imgid, 'full'); ?>
            <img src="<?php echo $image; ?>" />
            <?php endif ?>
        </div>
    </div>
</div>

<div class="solutions-bar">
    <div class="cage">
        <div class="table-container">
            <div class="table-content">
                <div class="label">
                    <?php echo apply_filters('the_title', $metas['casestudy_title'][0]); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="solutions-casestudy">
    <div class="cage">
        <div class="column col-left">
            <div class="title">
                <span><?php echo apply_filters('the_title', $metas['casestudy_problem_title'][0]); ?></span>
            </div>
            <?php
            $imgid = intval($metas['casestudy_problem_image'][0]);
            if ($imgid) :
                $image = wp_get_attachment_image_url($imgid, 'full');
            ?>
            <img src="<?php echo $image; ?>" />
            <?php endif; ?>
            <?php echo apply_filters('the_content', $metas['casestudy_problem_content'][0]); ?>
        </div><!--
        --><div class="column col-right">
            <div class="title">
                <span><?php echo apply_filters('the_title', $metas['casestudy_solution_title'][0]); ?></span>
            </div>
            <?php echo apply_filters('the_content', $metas['casestudy_solution_content'][0]); ?>
        </div>
    </div>
</div>

<?php
$imgid = $metas['scheme_image'][0];
if ($imgid) :
    $image = wp_get_attachment_image_url($imgid, 'full');
?>
<div class="solutions-image cage">
    <div class="image-container">
        <img src="<?php echo $image; ?>" />
        <div class="image-content">
            <?php
            $box = intval($metas['scheme_box'][0]);
            for ($i = 0; $i < $box; ++$i) {
                $content = apply_filters('the_content', $metas['scheme_box_' . $i . '_content'][0]);
                $left    = intval($metas['scheme_box_' . $i . '_pos_x'][0]);
                $top     = intval($metas['scheme_box_' . $i . '_pos_y'][0]);
                $width   = intval($metas['scheme_box_' . $i . '_width'][0]);
                $width   = $width == 0 ? 'auto' : $width . 'px';
                $align   = $metas['scheme_box_' . $i . '_align'][0];
                $content = str_replace('<p>', '<p style="text-align: ' . $align . '">', $content);

            ?>
            <div class="image-box" style="left: <?php echo $left; ?>px; top: <?php echo $top; ?>px; width: <?php echo $width; ?>;">
                <?php echo $content; ?>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php endif; ?>

<div class="solutions-bar">
    <div class="cage">
        <div class="table-container">
            <div class="table-content">
                <div class="label">
                    <?php echo apply_filters('the_title', $metas['atwork_title'][0]); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="solutions-atwork">
    <div class="cage"><?php
        $works = intval($metas['atwork_box'][0]);
        $atworks = array();
        for ($i = 0; $i < $works; ++$i) {
            $iconid  = intval($metas['atwork_box_' . $i . '_icon'][0]);
            $icon    = $iconid ? wp_get_attachment_image_url($iconid, true) : false;
            $title   = apply_filters('the_title', $metas['atwork_box_' . $i . '_title'][0]);
            $content = apply_filters('the_content', $metas['atwork_box_' . $i . '_content'][0]);
        ?><div class="column">
            <div class="atwork-head">
                <?php if ($icon) { ?><img src="<?php echo $icon; ?>" /><?php } ?><br/>
                <?php echo $title; ?>
            </div><!--
            --><div class="atwork-body">
                <?php echo $content; ?>
            </div>
        </div><?php
        }
    ?></div>
</div>

<?php /*
<div class="solutions-intro cage">
    <div class="intro-block">
        <h1><?php echo apply_filters('the_title', $metas['intro_title'][0]); ?></h1>
        <?php echo apply_filters('the_content', $metas['intro_content'][0]); ?>
    </div><div class="intro-block right">
        <?php
        $imgid = $metas['intro_image'][0];
        if ($imgid) :
            $image = wp_get_attachment_image_url($imgid, 'full');
        ?>
        <img src="<?php echo $image; ?>" />
        <?php endif; ?>
    </div>
</div>

<div class="solutions-box">
    <?php
    $box = intval($metas['pages_box'][0]);
    for ($i = 0; $i < $box; ++$i) {
        $page    = $metas['pages_box_' . $i . '_page'][0];
        $link    = get_permalink($page);
        $p_metas = get_post_meta($page);
        $title   = $p_metas['thumb_title'][0];
        $content = apply_filters('the_content', $p_metas['thumb_content'][0]);
        $imgid   = $p_metas['thumb_image'][0];
        $image   = $imgid ? wp_get_attachment_image_url($imgid, 'full') : false;
        $color   = $p_metas['thumb_color'][0];

    ?>
    <div class="box-container <?php echo $color; ?>">
        <div class="cage">
            <a href="<?php echo $link; ?>">
                <?php if ($i % 2 == 0) : // even: text on the left ?>
                <div class="box-content-text">
                    <div class="table-container">
                        <div class="table-content">
                            <h1><?php echo $title; ?></h1>
                            <?php echo $content; ?>
                        </div>
                    </div>
                </div><?php
                endif;
                ?><div class="box-content-image"<?php if ($image) { ?> style="background-image: url(<?php echo $image; ?>);"<?php } ?>>
                    <div class="box-content-alpha"></div>
                    <div class="table-container">
                        <div class="table-content">
                            <?php
                            $points  = intval($p_metas['thumb_points'][0]);
                            for ($j = 0; $j < $points; ++$j) {
                                $icoid = $p_metas['thumb_points_' . $j . '_icon'][0];
                                $icon  = $icoid ? wp_get_attachment_image_url($icoid, 'full') : false;
                                $label = $p_metas['thumb_points_' . $j . '_label'][0];

                            ?>
                            <div class="point-container">
                                <div class="point-icon"<?php if ($icon) { ?> style="background-image: url(<?php echo $icon; ?>);"<?php } ?>></div>
                                <div class="point-text">
                                    <?php echo $label; ?>
                                </div>
                            </div>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                </div><?php
                if ($i % 2 == 1) : // odd: text on the right
                ?><div class="box-content-text padding">
                    <div class="table-container">
                        <div class="table-content">
                            <h1><?php echo $title; ?></h1>
                            <?php echo $content; ?>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            </a>
        </div>
    </div>
    <?php } ?>
</div>

<?php
$keywords = intval($metas['keywords'][0]);
if ($keywords > 0) :
?>
<div class="kit-bar">
    <div class="cage">
        <div class="table-container">
            <div class="table-content">
                <ul class="keywords">
                    <?php for ($i = 0; $i < $keywords; ++$i) : ?>
                    <li><?php echo apply_filters('the_title', $metas['keywords_' . $i . '_word'][0]); ?></li>
                    <?php endfor; ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>

<?php include('includes/footer-nav.php'); ?>
*/ ?>

<?php endwhile; ?>

<?php get_footer(); ?>
