<?php
/**
 * Template Name: Solutions subpage Template
 *
 * @package WordPress
 * @subpackage CloudLeaf
 * @since CloudLeaf 1.0
 */

$id    = get_the_ID();
$metas = get_post_meta($id);

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<div class="solutions-intro cage">
    <div class="intro-block">
        <h1><?php echo apply_filters('the_title', $metas['intro_title'][0]); ?></h1>
        <?php echo apply_filters('the_content', $metas['intro_content'][0]); ?>
    </div><div class="intro-block right">
        <?php
        $imgid = $metas['intro_image'][0];
        if ($imgid) :
            $image = wp_get_attachment_image_url($imgid, 'full');
        ?>
        <img src="<?php echo $image; ?>" />
        <?php endif; ?>
    </div>
</div>


<div class="homepage-automotive solutions">
    <div class="cage">
        <h1><?php echo apply_filters('the_title', $metas['body_title'][0]); ?></h1>
        <div class="box-container">
            <div class="box-content wide">
                <?php echo apply_filters('the_content', $metas['body_column_left'][0]); ?>
            </div><div class="box-content wide">
                <?php echo apply_filters('the_content', $metas['body_column_right'][0]); ?>
            </div><div class="box-content">
                <h4>&nbsp;</h4>
                <div class="box-red left">
                    <?php echo apply_filters('the_content', $metas['body_box_content'][0]); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$slides = intval($metas['slides'][0]);
if ($slides > 0) :
?>
<div class="slides-container">
    <?php if ($slides > 1) { ?>
    <div class="slides-nav cage">
        <div class="button prev"></div>
        <div class="button next"></div>
    </div>
    <?php } ?>
    <div class="slides-content">
        <?php
        $max = $slides < 2 ? $slides : $slides < 6 ? $slides * 2 : $slides;
        for ($i = 0; $i < max($max, $slides); ++$i) {
            $index = $i % $slides;
            $imgid = $metas['slides_' . $index . '_slide'][0];
            $image = $imgid ? wp_get_attachment_image_url($imgid, 'full') : false;

        ?>
        <div class="slide<?php if ($i > 0) { ?> next<?php } ?>" data-index="<?php echo $i; ?>">
            <div class="table-container">
                <div class="table-content">
                    <?php if ($image) : ?>
                    <img src="<?php echo $image; ?>" />
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</div>
<?php endif; ?>

<?php
$imgid = $metas['scheme_image'][0];
if ($imgid) :
    $image = wp_get_attachment_image_url($imgid, 'full');
?>
<div class="solutions-image cage">
    <div class="image-container">
        <img src="<?php echo $image; ?>" />
        <div class="image-content">
            <?php
            $box = intval($metas['scheme_box'][0]);
            for ($i = 0; $i < $box; ++$i) {
                $content = apply_filters('the_content', $metas['scheme_box_' . $i . '_content'][0]);
                $left    = intval($metas['scheme_box_' . $i . '_pos_x'][0]);
                $top     = intval($metas['scheme_box_' . $i . '_pos_y'][0]);
                $width   = intval($metas['scheme_box_' . $i . '_width'][0]);
                $width   = $width == 0 ? 'auto' : $width . 'px';
                $align   = $metas['scheme_box_' . $i . '_align'][0];
                $content = str_replace('<p>', '<p style="text-align: ' . $align . '">', $content);

            ?>
            <div class="image-box" style="left: <?php echo $left; ?>px; top: <?php echo $top; ?>px; width: <?php echo $width; ?>;">
                <?php echo $content; ?>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php endif; ?>

<?php $kit = get_kit_menu(); ?>
<a href="<?php echo $kit->link; ?>">
    <div class="kit-bar">
        <div class="cage">
            <div class="table-container">
                <div class="table-content">
                    <div class="kit-link"><?php echo $kit->title; ?></div>
                </div>
            </div>
        </div>
    </div>
</a>

<?php include('includes/footer-nav.php'); ?>

<?php endwhile; ?>

<?php get_footer(); ?>
