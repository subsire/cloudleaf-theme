<?php
/**
 * Template Name: Contacts Template
 *
 * @package WordPress
 * @subpackage CloudLeaf
 * @since CloudLeaf 1.0
 */

$id    = get_the_ID();
$metas = get_post_meta($id);

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<div class="solutions-intro cage">
    <div class="intro-block">
        <h1><?php echo apply_filters('the_title', $metas['intro_title'][0]); ?></h1>
        <?php echo apply_filters('the_content', $metas['intro_content'][0]); ?>
    </div><div class="intro-block center">
        <?php
        $imgid = $metas['intro_image'][0];
        if ($imgid) :
            $image = wp_get_attachment_image_url($imgid, 'full');
        ?>
        <img src="<?php echo $image; ?>" />
        <?php endif; ?>
    </div>
</div>

<div class="contacts-container">
    <div class="contacts-menu">
        <div class="menu-container">
            <ul>
                <li>
                    <a href="#general-inquiry">General Inquiry</a>
                </li><li>
                    <a href="#request-a-kit">Request a kit</a>
                </li><li>
                    <a href="#careers">Careers</a>
                </li>
            </ul>
            <div class="form-active"></div>
        </div>
    </div>
    <div class="contacts-content">
        <div id="general-inquiry" class="form-container">
            <form id="general-inquiry-form" class="contacts-form" method="post" action="<?php echo get_permalink(); ?>">
                <div class="form-row">
                    <div class="form-cell label">
                        <label for="gi-name">Name</label>
                    </div><div class="form-cell input">
                        <input type="text" id="gi-name" name="name" class="form-input large" data-type="mandatory" />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-cell label">
                        <label for="gi-email">Email</label>
                    </div><div class="form-cell input">
                        <input type="email" id="gi-email" name="email" class="form-input large" data-type="mandatory" />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-cell label">
                        <label for="gi-phone-prefix">Phone number</label>
                    </div><div class="form-cell input">
                        <input type="text" id="gi-phone-prefix" name="phone-prefix" class="form-input small" value="+44" /><input type="text" id="gi-phone-number" name="phone-number" class="form-input" />
                    </div>
                </div>
                <div class="form-row">
                    <textarea id="gi-message" name="message" class="form-textarea" data-type="mandatory">Write your message</textarea>
                </div>
                <div class="form-row small center">
                    <label for="gi-terms">I accept the <a href="<?php echo get_permalink() ?>terms-of-use/" class="popup" target="_blank">terms of use</a> <input type="checkbox" id="gi-terms" name="terms" class="form-checkbox" value="1" data-type="mandatory" /></label>
                </div>
                <div class="form-row center">
                    <!-- CAPTCHA -->
                    <div id="gi-captcha" class="g-recaptcha" data-sitekey="6LcqiyQTAAAAAFx247C3BZ81mSk_8uZV3s454kO-"></div>
                </div>
                <div class="form-row center">
                    <input type="hidden" name="type" value="general_inquiry" />
                    <input type="submit" class="form-submit" value="SEND" />
                </div>
                <div class="form-row center message" data-ok="Message successfully sended" data-ko="Unable to send the message, try again later" data-error="Server error"></div>
            </form>
        </div>
        <div id="request-a-kit" class="form-container">
            <form id="request-a-kit-form" class="contacts-form" method="post" action="<?php echo get_permalink(); ?>">
                <div class="form-row">
                    <div class="form-cell label">
                        <label for="rak-name">Name</label>
                    </div><div class="form-cell input">
                        <input type="text" id="rak-name" name="name" class="form-input large" data-type="mandatory" />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-cell label">
                        <label for="rak-company">Company</label>
                    </div><div class="form-cell input">
                        <input type="text" id="rak-company" name="company" class="form-input large" data-type="mandatory" />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-cell label">
                        <label for="rak-email">Email</label>
                    </div><div class="form-cell input">
                        <input type="email" id="rak-email" name="email" class="form-input large" data-type="mandatory" />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-cell label">
                        <label for="rak-phone-prefix">Phone number</label>
                    </div><div class="form-cell input">
                        <input type="text" id="rak-phone-prefix" name="phone-prefix" class="form-input small" value="+44" /><input type="text" id="rak-phone-number" name="phone-number" class="form-input" />
                    </div>
                </div>
                <div class="form-row">
                    <textarea id="gi-message" name="message" class="form-textarea" data-type="mandatory">Write your message</textarea>
                </div>
                <div class="form-row small center">
                    <label for="rak-terms">I accept the <a href="<?php echo get_permalink() ?>terms-of-use/" class="popup" target="_blank">terms of use</a> <input type="checkbox" id="rak-terms" name="terms" class="form-checkbox" value="1" data-type="mandatory" /></label>
                </div>
                <div class="form-row center">
                    <!-- CAPTCHA -->
                    <div id="rak-captcha" class="g-recaptcha" data-sitekey="6LcqiyQTAAAAAFx247C3BZ81mSk_8uZV3s454kO-"></div>
                </div>
                <div class="form-row center">
                    <input type="hidden" name="type" value="request_a_kit" />
                    <input type="submit" class="form-submit" value="SEND" />
                </div>
                <div class="form-row center message" data-ok="Message successfully sended" data-ko="Unable to send the message, try again later" data-error="Server error"></div>
            </form>
        </div>
        <div id="careers" class="form-container">
            <form id="careers-form" class="contacts-form" method="post" action="<?php echo get_permalink(); ?>">
                <div class="form-row">
                    <div class="form-cell label">
                        <label for="c-name">Name</label>
                    </div><div class="form-cell input">
                        <input type="text" id="c-name" name="name" class="form-input large" data-type="mandatory" />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-cell label">
                        <label for="c-email">Email</label>
                    </div><div class="form-cell input">
                        <input type="email" id="c-email" name="email" class="form-input large" data-type="mandatory" />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-cell label">
                        <label for="c-job">Job</label>
                    </div><div class="form-cell input select">
                        <div class="form-span-select large">
                            <div class="table-container">
                                <div class="table-content" id="c-job-span"></div>
                            </div>
                        </div>
                        <select id="c-job" name="job" class="form-select large" data-type="mandatory">
                            <option></option>
                            <option value="Software engineering">Software engineering</option>
                            <option value="Hardware engineering">Hardware engineering</option>
                            <option value="Marketing">Marketing</option>
                            <option value="Sales">Sales</option>
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-cell label">
                        <label for="c-phone-prefix">Phone number</label>
                    </div><div class="form-cell input">
                        <input type="text" id="c-phone-prefix" name="phone-prefix" class="form-input small" value="+44" /><input type="text" id="c-phone-number" name="phone-number" class="form-input" />
                    </div>
                </div>
                <div class="form-row">
                    <textarea id="c-message" name="message" class="form-textarea" data-type="mandatory">Write your message</textarea>
                </div>
                <div class="form-row small center">
                    <label for="c-terms">I accept the <a href="<?php echo get_permalink() ?>terms-of-use/" class="popup" target="_blank">terms of use</a> <input type="checkbox" id="c-terms" name="terms" class="form-checkbox" value="1" data-type="mandatory" /></label>
                </div>
                <div class="form-row center">
                    <!-- CAPTCHA -->
                    <div id="c-captcha" class="g-recaptcha" data-sitekey="6LcqiyQTAAAAAFx247C3BZ81mSk_8uZV3s454kO-"></div>
                </div>
                <div class="form-row center">
                    <input type="hidden" name="type" value="careers" />
                    <input type="submit" class="form-submit" value="SEND" />
                </div>
                <div class="form-row center message" data-ok="Message successfully sended" data-ko="Unable to send the message, try again later" data-error="Server error"></div>
            </form>
        </div>
    </div>
</div>

<div class="contact-maps">
    <?php
    $coords = get_field('coordinates');
    $coords = array();
    if ((is_array($coords) && count($coords) == 0) || (!is_array($coords) && strlen($coords) == 0)) {
        // TODO: real coords... DEBUG -->
        $coords = array(
            'lat' => 37.436209,
            'lng' => -121.892015,
        );
    }
    ?>
    <div id="map" data-lat="<?php echo $coords['lat']; ?>" data-lng="<?php echo $coords['lng']; ?>">

    </div>
</div>

<?php endwhile; ?>

<?php get_footer(); ?>
