<?php
/**
 * Template Name: Homepage Template
 *
 * @package WordPress
 * @subpackage CloudLeaf
 * @since CloudLeaf 1.0
 */

$id    = get_the_ID();
$metas = get_post_meta($id);

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<div class="homepage-intro">
    <div class="cage">
        <div class="image">
            <?php
            $imgid = $metas['intro_image'][0];
            if ($imgid) :
                $image = wp_get_attachment_image_url($imgid, 'full');
            ?>
            <img src="<?php echo $image; ?>" />
            <?php endif; ?>
        </div><!--
        --><div class="content">
            <h1><?php echo apply_filters('the_title', $metas['intro_title'][0]); ?></h1>
            <?php echo apply_filters('the_content', $metas['intro_content'][0]); ?>
        </div>
    </div>
</div>

<div class="homepage-whatwedo cage">
    <div class="column">
        <h1><span><?php echo apply_filters('the_title', $metas['whatwedo_title'][0]); ?></span></h1>
        <?php echo apply_filters('the_content', $metas['whatwedo_content'][0]); ?>
    </div><!--
    --><div class="column">
        <div class="table-container">
            <div class="table-content">
                <?php
                $feats = intval($metas['wathwedo'][0]);
                if ($feats > 0) {
                    for ($i = 0; $i < $feats; ++$i) {
                        $icon_id = $metas['wathwedo_' . $i . '_icon'][0];


                    ?>
                    <div class="feat" id="feat-<?php echo $i; ?>">
                        <div class="feat-div feat-icon">
                            <?php if ($icon_id) :
                                $icon = wp_get_attachment_image_url($icon_id, 'full');
                            ?>
                            <img src="<?php echo $icon; ?>" />
                            <?php endif; ?>
                        </div>
                        <div class="feat-div feat-name">
                            <div class="table-container">
                                <div class="table-content">
                                    <?php echo $metas['wathwedo_' . $i . '_title'][0]; ?>
                                </div>
                            </div>
                        </div>
                        <div class="feat-div feat-button"></div>
                        <div class="feat-content"><?php echo apply_filters('the_content', $metas['wathwedo_' . $i . '_content'][0]); ?></div>
                    </div>
                    <?php
                    }
                }
                ?>
            </div>
        </div>
    </div>
</div>

<?php
$features = intval($metas['features'][0]);
if ($features) : ?>
<div class="homepage-features">
    <div class="cage"><!--
        <?php for ($i = 0; $i < $features; ++$i) : ?>
        --><div class="column">
            <?php
            $imgid = $metas['features_' . $i . '_image'][0];
            if ($imgid) :
                $image = wp_get_attachment_image_url($imgid, 'full'); ?>
            <img src="<?php echo $image; ?>" />
            <?php endif; ?>
            <div class="content">
                <span class="title"><?php echo apply_filters('the_title', $metas['features_' . $i . '_title'][0]); ?></span>
                <?php echo apply_filters('the_content', $metas['features_' . $i . '_content'][0]); ?>
            </div>
        </div><!--
        <?php endfor; ?>
    --></div>
</div>
<?php endif; ?>

<div class="homepage-customers">
    <div class="background"></div>
    <div class="container cage">
        <div class="column">
            <div class="slide-container">
                <?php
                $quotes = intval($metas['customers_quotes'][0]);
                for ($i = 0; $i < $quotes; ++$i) :
                    $content = str_replace(array('<p>', '</p>'), '', apply_filters('the_content', $metas['customers_quotes_' . $i . '_content'][0]));
                    $source  = str_replace(array('<p>', '</p>'), '', apply_filters('the_content', $metas['customers_quotes_' . $i . '_source'][0]));
                ?>
                <div class="slide-content slide-<?php echo $i; ?>" data-index="<?php echo $i; ?>">
                    <div class="table-container">
                        <div class="table-content">
                            <div class="slide-quote"><?php echo $content; ?></div>
                            <div class="slide-source"><?php echo $source; ?></div>
                        </div>
                    </div>
                </div>
                <?php endfor; ?>
                <?php if ($quotes > 1) : ?>
                <div class="slide-index-cont">
                    <?php for ($i = 0; $i < $quotes; ++$i) : ?>
                    <div class="slide-index<?php if ($i == 0) echo " selected"; ?>" data-index="<?php echo $i; ?>"></div>
                    <?php endfor; ?>
                </div>
                <?php endif; ?>
            </div>
        </div><!--
        --><div class="column">
            <div class="customers-title">
                <div class="table-container">
                    <div class="table-content">
                        <?php echo str_replace(array('<p>', '</p>'), array('<h1>', '</h1>'), apply_filters('the_content', $metas['customers_content'][0])); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="homepage-keymarkets">
    <div class="cage">
        <div class="column">
            <div class="table-container">
                <div class="table-content">
                    <div class="keymarkets-title">
                        <?php echo str_replace(array('<p>', '</p>'), array('<h1>', '</h1>'), apply_filters('the_content', $metas['keymarkets_content'][0])); ?>
                    </div>
                </div>
            </div>
        </div><!--
        --><div class="column">
            <div class="table-container">
                <div class="table-content"><!--
                    <?php
                    $markets = intval($metas['keymarkets'][0]);
                    for ($i = 0; $i < $markets; ++$i) {
                    ?>
                    --><div class="keymarket">
                    <?php
                        $disabled = boolval($metas['keymarkets_' . $i . '_disabled'][0]);
                        if (!$disabled) :
                            $imgid = $metas['keymarkets_' . $i . '_icon'][0];
                            $image = $imgid ? wp_get_attachment_image_url($imgid, 'full') : false;
                            $name  = apply_filters('the_title', $metas['keymarkets_' . $i . '_name'][0]);
                        ?>
                        <?php if ($image) { ?><img src="<?php echo $image; ?>" /><?php } ?>
                        <div class="label"><?php echo $name; ?></div>
                        <?php
                        endif;
                    ?>
                    </div><!--
                    <?php
                    }
                    ?>
                --></div>
            </div>
        </div>
    </div>
</div>

<?php endwhile; ?>

<?php get_footer(); ?>
