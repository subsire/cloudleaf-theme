<?php
/*
 * Footer nav
 *
 */
?>

<?php if (boolval($metas['solutions_enabled'][0])) : ?>
<div class="page-solutions cage">
    <?php
    $solutions = intval($metas['solutions_box'][0]);
    for ($i = 0; $i < $solutions; ++$i) {
        $page  = $metas['solutions_box_' . $i . '_page'][0];
        $title = get_the_title($page);
        $link  = get_permalink($page);
        $imgid = $metas['solutions_box_' . $i . '_image'][0];
        $image = $imgid ? wp_get_attachment_image_url($imgid, 'cloudleaf_1_3') : false;
        $label = $metas['solutions_box_' . $i . '_label'][0];

    ?><div id="page-<?php echo $page; ?>" class="page-box">
        <?php if ($image) : ?>
        <img src="<?php echo $image; ?>" />
        <?php endif; ?>
        <a href="<?php echo $link; ?>">
            <div class="page-abs">
                <div class="table-container">
                    <div class="table-content">
                        <h3><?php echo apply_filters('the_title', $title); ?></h3>
                        <?php if (strlen($label)) : ?>
                        <div class="page-label"><?php echo $label; ?></div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </a>
    </div><?php
    }
    ?>
</div>
<?php endif; ?>

<?php if (boolval($metas['team_enabled'][0])) : ?>
<div class="page-team cage">
    <div class="team-title">
        <div class="table-container">
            <div class="table-content">
                <h1><?php echo apply_filters('the_title', $metas['team_title'][0]); ?></h1>
            </div>
        </div>
    </div>
    <div class="team-content">
        <div class="content-title">
            <h3><?php echo apply_filters('the_title', $metas['team_box_title'][0]); ?></h3>
        </div>
        <div class="content-container">
            <?php
            $box = intval($metas['team_box'][0]);
            for ($i = 0; $i < $box; ++$i) {
                $imgid   = $metas['team_box_' . $i . '_icon'][0];
                $image   = $imgid ? wp_get_attachment_image_url($imgid, 'full') : false;
                $content = apply_filters('the_content', $metas['team_box_' . $i . '_content'][0]);

            ?><div class="team-box">
                <?php if ($image) : ?>
                <img src="<?php echo $image; ?>" />
                <?php endif; ?>
                <?php echo $content; ?>
            </div><?php
            }
            ?>
        </div>
    </div>
</div>
<?php endif; ?>
