<?php
/**
 * Template Name: Company Template
 *
 * @package WordPress
 * @subpackage CloudLeaf
 * @since CloudLeaf 1.0
 */

$id    = get_the_ID();
$metas = get_post_meta($id);

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<div class="company-team">
    <div class="cage">
        <div class="column">
            <div class="table-container">
                <div class="table-content">
                    <h1><?php echo apply_filters('the_title', $metas['intro_title'][0]); ?></h1>
                </div>
            </div>
        </div><!--
        --><div class="column">
            <div class="table-container">
                <div class="table-content">
                    <?php echo apply_filters('the_content', $metas['intro_content'][0]); ?>
                </div>
            </div>
        </div>
    </div>

    <?php /*
    <div class="intro-block">
        <h1><?php echo apply_filters('the_title', $metas['intro_title'][0]); ?></h1>
        <?php echo apply_filters('the_content', $metas['intro_content'][0]); ?>
    </div><div class="intro-block right bottom">
        <?php
        $imgid = $metas['intro_image'][0];
        if ($imgid) :
            $image = wp_get_attachment_image_url($imgid, 'full');
        ?>
        <img src="<?php echo $image; ?>" />
        <?php endif; ?>
    </div>
    */ ?>
</div>

<div class="company-tbd">
    <div class="cage">
        <div class="column">
            <div class="table-container">
                <div class="table-content">
                    <?php
                    $imgid = intval($metas['selfies_image'][0]);
                    if ($imgid) :
                        $image = wp_get_attachment_image_url($imgid, 'full');
                    ?>
                    <img src="<?php echo $image; ?>" />
                    <?php endif; ?>
                </div>
            </div>
        </div><!--
        --><div class="column">
            <div class="table-container">
                <div class="table-content">
                    <?php echo apply_filters('the_content', $metas['selfies_content'][0]); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php /*
<div class="company-team grey">
    <div class="cage">
        <?php
        $members = intval($metas['team_members'][0]);
        if ($members > 0) :
        ?>
        <div class="team-container">
            <div class="team-content">
                <?php
                for ($i = 0; $i < $members; ++$i) {
                    $imgid   = $metas['team_members_' . $i . '_image'][0];
                    $image   = $imgid ? wp_get_attachment_image_url($imgid, 'cloudleaf_thumb') : false;
                    $content = apply_filters('the_content', $metas['team_members_' . $i . '_content'][0]);

                ?><div class="team-member">
                    <?php if ($image) { ?><img src="<?php echo $image; ?>" /><?php } ?>
                    <div class="member-content">
                        <div class="content-text">
                            <?php echo $content; ?>
                        </div>
                    </div>
                </div><?php
                }
                ?>
            </div>
        </div>
        <?php endif; ?>
    </div>
</div>

<?php $kit = get_kit_menu('#careers'); ?>
<a href="<?php echo $kit->link; ?>">
    <div class="kit-bar">
        <div class="cage">
            <div class="table-container">
                <div class="table-content">
                    <div class="kit-link">We are hiring</div>
                </div>
            </div>
        </div>
    </div>
</a>

<div class="company-team cage">
    <h1><?php echo apply_filters('the_title', $metas['management_title'][0]); ?></h1>
    <?php
    $members = intval($metas['management_board'][0]);
    if ($members > 0) : ?>
    <div class="team-container">
        <?php
        for ($i = 0; $i < $members; ++$i) {
            $imgid   = $metas['management_board_' . $i . '_image'][0];
            $image   = $imgid ? wp_get_attachment_image_url($imgid, 'cloudleaf_thumb') : false;
            $content = apply_filters('the_content', $metas['management_board_' . $i . '_content'][0]);

        ?><div class="team-member wide">
            <?php if ($image) { ?><img src="<?php echo $image; ?>" /><?php } ?>
            <div class="member-content">
                <div class="content-text">
                    <?php echo $content; ?>
                </div>
            </div>
        </div><?php
        }
        ?>
    </div>
    <?php endif; ?>
</div>

<div class="company-investors cage">
    <h1><?php echo apply_filters('the_title', $metas['investors_title'][0]); ?></h1>
    <?php
    $investors = intval($metas['investors'][0]);
    if ($investors > 0) : ?>
    <ul>
        <?php
        for ($i = 0; $i < $investors; ++$i) {
            $imgid = $metas['investors_' . $i . '_image'][0];
            if ($imgid) {
                $image = wp_get_attachment_image_url($imgid, 'full');
                $link  = $metas['investors_' . $i . '_link'][0];

        ?>
        <?php if (strlen($link) > 0) { ?><a href="<?php echo $link; ?>" target="_blank"><?php } ?>
            <li><img src="<?php echo $image; ?>" /></li>
        <?php if (strlen($link) > 0) { ?></a><?php } ?>
        <?php
            }
        }
        ?>
    </ul>
    <?php endif; ?>
</div>

<?php include('includes/footer-nav.php'); ?>
*/ ?>

<?php endwhile; ?>

<?php get_footer(); ?>
