<?php
/**
 * Template Name: Technology Template
 *
 * @package WordPress
 * @subpackage CloudLeaf
 * @since CloudLeaf 1.0
 */

$id    = get_the_ID();
$metas = get_post_meta($id);

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<div class="technology-intro cage">
    <div class="cage">
        <div class="column">
            <div class="table-container">
                <div class="table-content">
                    <?php
                    $imgid = $metas['intro_image'][0];
                    if ($imgid) :
                        $image = wp_get_attachment_image_url($imgid, 'full');
                    ?>
                    <img src="<?php echo $image; ?>" class="intro-img" />
                    <?php endif; ?>
                </div>
            </div>
        </div><!--
        --><div class="column">
            <div class="intro-content">
                <h1><?php echo apply_filters('the_title', $metas['intro_title'][0]); ?></h1>
                <?php echo apply_filters('the_content', $metas['intro_content'][0]); ?>
            </div>
        </div>
    </div>
</div>

<div class="technology-platform">
    <div class="cage">
        <h1><?php echo apply_filters('the_title', $metas['platform_title'][0]); ?></h1>
        <?php
        $imgid = $metas['platform_image'][0];
        if ($imgid) :
            $image = wp_get_attachment_image_url($imgid, 'full');
        ?>
        <div class="image-container">
            <img src="<?php echo $image; ?>" />
            <div class="image-content">
                <?php
                $box = intval($metas['platform_box'][0]);
                for ($i = 0; $i < $box; ++$i) {
                    $content = apply_filters('the_content', $metas['platform_box_' . $i . '_content'][0]);
                    $left    = intval($metas['platform_box_' . $i . '_pos_x'][0]);
                    $top     = intval($metas['platform_box_' . $i . '_pos_y'][0]);
                    $width   = intval($metas['platform_box_' . $i . '_width'][0]);
                    $width   = $width == 0 ? 'auto' : $width . 'px';
                    $align   = $metas['platform_box_' . $i . '_align'][0];
                    $content = str_replace('<p>', '<p style="text-align: ' . $align . '">', $content);

                ?>
                <div class="image-box" style="left: <?php echo $left; ?>px; top: <?php echo $top; ?>px; width: <?php echo $width; ?>;">
                    <?php echo $content; ?>
                </div>
                <?php } ?>
            </div>
        </div>
        <?php endif; ?>
    </div>
</div>

<?php /*
<div class="technology-hardware cage">
    <div class="box-title">
        <h1><?php echo apply_filters('the_title', $metas['hardware_title'][0]); ?></h1>
    </div>
    <?php
    $imgid = $metas['hardware_image'][0];
    if ($imgid) :
        $image = wp_get_attachment_image_url($imgid, 'full');
    ?>
    <div class="image-container">
        <img src="<?php echo $image; ?>" />
        <div class="image-content">
            <?php
            $box = intval($metas['hardware_box'][0]);
            for ($i = 0; $i < $box; ++$i) {
                $content = apply_filters('the_content', $metas['hardware_box_' . $i . '_content'][0]);
                $left    = intval($metas['hardware_box_' . $i . '_pos_x'][0]);
                $top     = intval($metas['hardware_box_' . $i . '_pos_y'][0]);
                $class   = $metas['hardware_box_' . $i . '_position'][0];

            ?>
            <div class="image-box box-baloon" style="left: <?php echo $left; ?>px; top: <?php echo $top; ?>px;">
                <img src="<?php echo get_template_directory_uri(); ?>/img/plus.png" />
                <div class="baloon-container <?php echo $class; ?>"><?php echo $content; ?></div>
            </div>
            <?php } ?>
        </div>
    </div>
    <?php endif; ?>
</div>
*/ ?>

<div class="technology-cloud">
    <div class="cage">
        <div class="cloud-image">
            <?php
            $imgid = $metas['cloud_image'][0];
            if ($imgid) :
                $image = wp_get_attachment_image_url($imgid, 'full');
            ?>
            <img src="<?php echo $image; ?>" />
            <?php endif; ?>
        </div><div class="cloud-content">
            <div class="table-container">
                <div class="table-content">
                    <div class="cloud-text">
                        <h1><?php echo apply_filters('the_title', $metas['cloud_title'][0]); ?></h1>
                        <?php echo apply_filters('the_content', $metas['cloud_content'][0]); ?>
                    </div>
                    <div class="cloud-icons-container">
                        <div class="cloud-icons">
                            <?php
                            $icons = intval($metas['cloud_icons'][0]);
                            for ($i = 0; $i < $icons; ++$i) {
                                $iconid = $metas['cloud_icons_' . $i . '_image'][0];
                                $icon   = $iconid ? wp_get_attachment_image_url($iconid, 'full') : false;
                            ?><div class="cloud-icon" data-index="<?php echo $i; ?>">
                                <div class="table-container">
                                    <div class="table-content">
                                        <?php if ($icon) : ?><img src="<?php echo $icon; ?>" /><?php endif; ?>
                                        <h4><?php echo apply_filters('the_title', $metas['cloud_icons_' . $i . '_label'][0]); ?></h4>
                                    </div>
                                </div>
                            </div><div class="icon-text-container">
                                <div class="icon-text-content">
                                    <div class="table-container">
                                        <div class="table-content">
                                            <?php echo apply_filters('the_content', $metas['cloud_icons_' . $i . '_content'][0]); ?>
                                        </div>
                                    </div>
                                </div>
                            </div><?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="technology-slides">
    <div class="cage">
        <div class="slide-container">
            <?php
            $slides = intval($metas['slides'][0]);
            for ($i = 0; $i < $slides; $i++) :
                $imgid = intval($metas['slides_' . $i . '_image'][0]);
                $image = $imgid ? wp_get_attachment_image_url($imgid, 'full') : false;
                $title = apply_filters('the_title', $metas['slides_' . $i . '_title'][0]);
            ?>
            <div class="slide-content slide-<?php echo $i; ?>" data-index="<?php echo $i; ?>">
                <div class="table-container">
                    <div class="table-content">
                        <?php if ($image) : ?><div class="slide-image"><img src="<?php echo $image; ?>" /></div><?php endif; ?>
                        <div class="slide-title"><?php echo $title; ?></div>
                    </div>
                </div>
            </div>
            <?php endfor; ?>
            <?php if ($slides > 1) : ?>
            <div class="slide-arrow prev"></div>
            <div class="slide-arrow next"></div>
            <?php endif; ?>
        </div>
    </div>
</div>

<div class="technology-devices">
    <div class="column clm-left">
        <div class="semi-cage">
            <div class="slide-container">
                <?php
                $devices = intval($metas['devices'][0]);
                for ($i = 0; $i < $devices; $i++) :
                    $imgid = intval($metas['devices_' . $i . '_image'][0]);
                    $image = $imgid ? wp_get_attachment_image_url($imgid, 'full') : false;
                    $label = apply_filters('the_title', $metas['devices_' . $i . '_label'][0]);
                    $feat1 = apply_filters('the_title', $metas['devices_' . $i . '_feature_1'][0]);
                    $feat2 = apply_filters('the_title', $metas['devices_' . $i . '_feature_2'][0]);
                    $feat3 = apply_filters('the_title', $metas['devices_' . $i . '_feature_3'][0]);
                ?>
                <div class="slide-content slide-<?php echo $i; ?>" data-index="<?php echo $i; ?>">
                    <div class="table-container">
                        <div class="table-content">
                            <?php if ($image) : ?><div class="slide-image"><img src="<?php echo $image; ?>" /></div><?php endif; ?>
                            <div class="slide-label"><?php echo $label; ?></div>
                            <div class="slide-feats">
                                <div class="feat-icon"><div class="table-container"><div class="table-content"><?php echo $feat1; ?></div></div></div>
                                <div class="feat-icon"><div class="table-container"><div class="table-content"><?php echo $feat2; ?></div></div></div>
                                <div class="feat-icon last"><div class="table-container"><div class="table-content"><?php echo $feat3; ?></div></div></div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endfor; ?>
                <?php if ($devices > 1) : ?>
                <div class="slide-arrow prev"></div>
                <div class="slide-arrow next"></div>
                <?php endif; ?>
            </div>
        </div>
    </div><!--
    --><div class="column clm-right">
        <div class="semi-cage">
            <div class="table-container">
                <div class="table-content">
                    <h1><span><?php echo apply_filters('the_title', $metas['devices_title'][0]); ?></span></h1>
                    <?php echo apply_filters('the_content', $metas['devices_content'][0]); ?>
                    <?php
                    $imgid = intval($metas['devices_image'][0]);
                    $image = $imgid ? wp_get_attachment_image_url($imgid, 'full') : false;
                    if ($image) : ?>
                    <img src="<?php echo $image; ?>" />
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php endwhile; ?>

<?php get_footer(); ?>
